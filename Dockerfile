FROM ubuntu:22.04

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt -y update && \
    apt -y upgrade && \
    apt -y install --no-install-recommends \
    autoconf automake autopoint bash bison build-essential bzip2 flex \
    g++ g++-multilib gettext git gperf intltool iputils-ping libc6-dev-i386 \
    libffi-dev libgdk-pixbuf2.0-dev libltdl-dev libssl-dev libtool-bin \
    libxml-parser-perl lzip make nano openssl p7zip-full patch perl \
    pkg-config ruby scons sed unzip wget xz-utils libgl-dev \
    python3 python3-mako python3-pkg-resources python3-setuptools python-is-python3 \
    && \
    apt -y autoremove && \
    apt -y autoclean && \
    apt -y clean && \
    rm -rf /var/lib/apt/lists/* && \
    exit 0

# see http://stackoverflow.com/questions/10934683/how-do-i-configure-qt-for-cross-compilation-from-linux-to-windows-target
# Preapre and download cross development environment
RUN mkdir /build
WORKDIR  /build
RUN git clone https://github.com/mxe/mxe.git

# Build cross environment
RUN cd mxe && make qtbase
RUN cd mxe && make qtmultimedia

# TODO: Cleanup all unneeded stuff to make a slim image

# Enhance path
ENV PATH /build/mxe/usr/bin:$PATH

# Add a qmake alias
RUN ln -s /build/mxe/usr/bin/i686-w64-mingw32.static-qmake-qt5 /build/mxe/usr/bin/qmake
